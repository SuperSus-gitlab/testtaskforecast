import React from 'react';
import cx from 'clsx';
import { City } from '../../types';
import getWeather from '../helpers/getWeather';
import { ForecastContext } from '../../context/ForecastContext';
import styles from './menu.module.less';

const cities = ['Ottawa', 'Toronto', 'Vancouver'];

class Menu extends React.Component {
  static contextType = ForecastContext;
  declare context: React.ContextType<typeof ForecastContext>;

  onClickHandler = (event: React.MouseEvent<HTMLButtonElement>) => {
    const button: HTMLButtonElement = event.currentTarget;
    const city = button.value as City;

    this.context?.setCity(city);
    getWeather(city, () => {
      this.context?.setLoading(true);
    }).then((response: ObjectLiteral) => {
      const { data } = response;
      
      this.context?.setForecast(data.response[0].periods);
    }).catch((error) => {
      console.log(error);
    }).finally(() => {
      this.context?.setLoading(false);
    });
  };

  render() {
    return (
      <div className={styles.menu}>
        {cities.map((item: string, i: number) => (
          <button
            key={`navigation-${i}`}
            className={cx(styles.cta, this.context?.city === item.toLowerCase() && styles.activeCta)}
            onClick={this.onClickHandler}
            value={item.toLowerCase()}
          >
            {item}
          </button>
        ))}
      </div>
    );
  }
}

export default Menu;
