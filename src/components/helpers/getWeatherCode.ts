
import { faSnowflake, faSmog, faCloudRain, faSun } from '@fortawesome/free-solid-svg-icons';

const getWeatherCode = (weatherCodes: string) => {
  const weatherCodeArray = weatherCodes.split(':');
  const weatherCode = weatherCodeArray[weatherCodeArray.length - 1];

  switch (weatherCode) {
    case 'A': case 'BS': case 'BY': case 'IC': case 'IP': case 'SI': case 'RS': case 'WM': case 'S': case 'SW':
      return faSnowflake;
    case 'BR': case 'F': case 'FR': case 'H': case 'IF': case 'K': case 'ZF':
      return faSmog;
    case 'P': case 'L': case 'T': case 'ZL': case 'ZR': case 'ZY': case 'RW':
      return faCloudRain;
    default:
      return faSun;
  }
}

export default getWeatherCode;
