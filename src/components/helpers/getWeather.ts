import axios from 'axios';
import { City } from '../../types';

const getWeather = (city: City, setLoading: (value: boolean) => void) => {
  setLoading(true);

  const options = {
    method: 'GET',
    url: `https://aerisweather1.p.rapidapi.com/forecasts/${city},ca`,
    params: { plimit: 5 },
    headers: {
      'X-RapidAPI-Key': '3fa65fbee9msh537673ee79da5ecp1859adjsn50ab1a301867',
      'X-RapidAPI-Host': 'aerisweather1.p.rapidapi.com'
    }
  };

  return axios.request(options);
}

export default getWeather;