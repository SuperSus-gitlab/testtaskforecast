import React from 'react';
import getWeather from '../helpers/getWeather';
import getWeatherCode from '../helpers/getWeatherCode';
import { ForecastContext } from '../../context/ForecastContext';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import styles from './weather.module.less';

class Weather extends React.Component {
  static contextType = ForecastContext;
  declare context: React.ContextType<typeof ForecastContext>;
  private days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

  componentDidMount() {
    const city = this.context?.city;

    if (city) {
      getWeather(city, () => {
        this.context?.setLoading(true);
      }).then((response: ObjectLiteral) => {
        const { data } = response;
        
        this.context?.setForecast(data.response[0].periods);
      }).catch((error) => {
        console.log(error);
      }).finally(() => {
        this.context?.setLoading(false);
      });
    }
  }

  render() {
    const today = this.context?.forecast[0];

    return (
      <div className={styles.weatherWrapper}>
        {this.context?.loading && (
          <div className={styles.loading}>
            <FontAwesomeIcon className={styles.spinner} icon={faSpinner} size="4x" />
          </div>
        )}
        <div className={styles.today}>
          <div className={styles.title}>Today</div>
          <div className={styles.currentWeather}>
            <div className={styles.currentCondition}>
              {today && <FontAwesomeIcon icon={getWeatherCode(today?.weatherPrimaryCoded)} size="6x" />}
            </div>
            <div>
              <div className={styles.currentTemperature}>{today?.avgTempC}°</div>
              <div className={styles.currentDescription}>{today?.weatherPrimary}</div>
            </div>
          </div>
        </div>
        <div className={styles.forecast}>
          {this.context?.forecast.map((item: ObjectLiteral, i: number) => {
            if (i === 0) return;
            const day = new Date(item.timestamp * 1000).getDay();
            
            return (
              <div className={styles.forecastItem} key={`forecast-${i}`}>
                <div className={styles.forecastDay}>{this.days[day]}</div>
                <div className={styles.forecasstIcon}>
                  <FontAwesomeIcon icon={getWeatherCode(item.weatherPrimaryCoded)} size="3x" />
                </div>
                <div className={styles.forecastTemperature}>{item.avgTempC}°</div>
              </div>
            )
          })}
        </div>
      </div>
    );
  }
}

export default Weather;
