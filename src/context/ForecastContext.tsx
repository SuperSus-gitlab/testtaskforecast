import React, { createContext } from 'react';
import { City } from '../types';

interface ForecastContextI {
  children: React.ReactNode;
}

type ForecastContextType = {
  city: City;
  setCity: (value: City) => void;
  forecast: ObjectLiteral[];
  setForecast: (value: ObjectLiteral[]) => void;
  loading: boolean;
  setLoading: (value: boolean) => void;
};

export const ForecastContext = createContext<ForecastContextType | null>(null);

export class ForecastContextProvider extends React.Component<ForecastContextI> {
  state = {
    city: 'ottawa' as City,
    forecast: [],
    loading: false,
  };

  setCity = (city: City) => {
    this.setState({ city });
  };

  setForecast = (forecast: ObjectLiteral[]) => {
    this.setState({ forecast });
  };

  setLoading = (loading: boolean) => {
    this.setState({ loading });
  };

  render() {
    return (
      <ForecastContext.Provider
        value={{
          city: this.state.city,
          setCity: this.setCity,
          forecast: this.state.forecast,
          setForecast: this.setForecast,
          loading: this.state.loading,
          setLoading: this.setLoading,
        }}
      >
        {this.props.children}
      </ForecastContext.Provider>
    );
  }
}
