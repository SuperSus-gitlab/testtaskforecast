export type Maybe<T> = T | undefined | null;

export type City = 'ottawa' | 'toronto' | 'vancouver';
