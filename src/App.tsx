import React, { createContext } from 'react';
import Menu from './components/Menu/Menu';
import Weather from './components/Weather/Weather';
import { ForecastContextProvider } from './context/ForecastContext';
import styles from './app.module.less';

class App extends React.Component {
  render() {
    return (
      <ForecastContextProvider>
        <div className={styles.wrapper}>
          <div className={styles.weatherBlock}>
            <Menu />
            <Weather />
          </div>
        </div>
      </ForecastContextProvider>
    );
  }
}

export default App;
