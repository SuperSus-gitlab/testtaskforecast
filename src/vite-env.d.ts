/// <reference types="vite/client" />

declare interface ObjectLiteral {
  [key: string]: any;
}
