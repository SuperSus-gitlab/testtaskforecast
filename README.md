# Technologies
1. Vite as a bundler
2. React (class components)
3. Typescript
4. LESS modules
5. Axios

# How to run locally
1. pnpm install / yarn
2. pnpm run dev / yarn dev

# Ways to improve code
1. Move the "getWeather" logic used in the "Weather" and "Menu" components to a helper to avoid duplicating code.
2. Write unit tests.
3. Consider using server-side rendering (SSR) as a possible approach for this task.
4. Secure the API key by storing it in the ".env" file or in a vault.
5. Add an interface for the data received from the backend.
6. Add a weather icon with clouds (depending on the percentage of the sky that is covered by clouds). Unfortunately, this can only be done on the pro version of the FontAwesome website.

# Link
https://testtaskforecast.vercel.app/
